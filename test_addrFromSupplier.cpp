#include "modbus.h"
#include "arduPi.h"
#include <string>

using namespace std;

string inverter_IP;
int inverter_addr;
int inverter_number;
int total_variables = 31;

typedef struct ABB_Inverter {
  double energy;                           //uint32_t addr_energy = 40094;
                                           uint32_t addr_energy = 40001;
  double meanOutputCurrent;               uint32_t addr_meanOutputCurrent = 40072;
  double phaseACurrent;                   uint32_t addr_phaseACurrent = 40073;
  double phaseBCurrent;                   uint32_t addr_phaseBCurrent = 40074;
  double phaseCCurrent;                   uint32_t addr_phaseCCurrent = 40075;
  double phaseVoltageAB;                  uint32_t addr_phaseVoltageAB = 40077;
  double phaseVoltageBC;                  uint32_t addr_phaseVoltageBC = 40078;
  double phaseVoltageCA;                  uint32_t addr_phaseVoltageCA = 40079;
  double phaseVoltageAN;                  uint32_t addr_phaseVoltageAN = 40080;
  double phaseVoltageBN;                  uint32_t addr_phaseVoltageBN = 40081;
  double phaseVoltageCN;                  uint32_t addr_phaseVoltageCN = 40082;
  double outputActivePowerInstant;        uint32_t addr_outputActivePowerInstant = 40084;
  double reactivePower;                   uint32_t addr_reactivePower = 40090;
  double apparentPower;                   uint32_t addr_apparentPower = 40088;
  double powerFactor;                     uint32_t addr_powerFactor = 40092;
  double meanGridFrequency;               uint32_t addr_meanGridFrequency = 40086;
  double inputCurrent;                    uint32_t addr_inputCurrent = 40097;;
  double activePowerDCInput1;             uint32_t addr_activePowerDCInput1 = 41125;
  double voltageDCInput1;                 uint32_t addr_voltageDCInput1 = 41124;
  double currentDCInput1;                 uint32_t addr_currentDCInput1 = 41123;
  double activePowerDCInput2;             uint32_t addr_activePowerDCInput2 = 41145;
  double voltageDCInput2;                 uint32_t addr_voltageDCInput2 = 41144;
  double currentDCInput2;                 uint32_t addr_currentDCInput2 = 41143;
  double activePowerDCInput3;             uint32_t addr_activePowerDCInput3 = 41165;
  double voltageDCInput3;                 uint32_t addr_voltageDCInput3 = 41164;
  double currentDCInput3;                 uint32_t addr_currentDCInput3 = 41163;
  double cabinetTemp;                     uint32_t addr_cabinetTemp = 40103;
  double heatSinkTemp;                    uint32_t addr_heatSinkTemp = 40104;
  double transformerTemp;                 uint32_t addr_transformerTemp = 40105;
  double externalTemp;                    uint32_t addr_externalTemp = 40106;
  double maxLeakageCurrent;               uint32_t addr_maxLeakageCurrent = 1128;
  //check type for timestamp: 41128
} Inverter;
Inverter ABB;

double all_variables[31] = {ABB.energy, ABB.meanOutputCurrent, ABB.phaseACurrent, ABB.phaseBCurrent, ABB.phaseCCurrent, ABB.phaseVoltageAB, ABB.phaseVoltageBC, ABB.phaseVoltageCA, ABB.phaseVoltageAN, ABB.phaseVoltageBN, ABB.phaseVoltageCN, ABB.outputActivePowerInstant, ABB.reactivePower, ABB.apparentPower, ABB.powerFactor, ABB.meanGridFrequency, ABB.inputCurrent, ABB.activePowerDCInput1, ABB.activePowerDCInput2, ABB.activePowerDCInput3, ABB.voltageDCInput1, ABB.voltageDCInput2, ABB.voltageDCInput3, ABB.currentDCInput1, ABB.currentDCInput2, ABB.currentDCInput3, ABB.cabinetTemp, ABB.heatSinkTemp, ABB.transformerTemp, ABB.externalTemp, ABB.maxLeakageCurrent};

uint32_t all_addr[31] = {ABB.addr_energy, ABB.addr_meanOutputCurrent, ABB.addr_phaseACurrent, ABB.addr_phaseBCurrent, ABB.addr_phaseCCurrent, ABB.addr_phaseVoltageAB, ABB.addr_phaseVoltageBC, ABB.addr_phaseVoltageCA, ABB.addr_phaseVoltageAN, ABB.addr_phaseVoltageBN, ABB.addr_phaseVoltageCN, ABB.addr_outputActivePowerInstant, ABB.addr_reactivePower, ABB.addr_apparentPower, ABB.addr_powerFactor, ABB.addr_meanGridFrequency, ABB.addr_inputCurrent, ABB.addr_activePowerDCInput1, ABB.addr_activePowerDCInput2, ABB.addr_activePowerDCInput3, ABB.addr_voltageDCInput1, ABB.addr_voltageDCInput2, ABB.addr_voltageDCInput3, ABB.addr_currentDCInput1, ABB.addr_currentDCInput2, ABB.addr_currentDCInput3, ABB.addr_cabinetTemp, ABB.addr_heatSinkTemp, ABB.addr_transformerTemp, ABB.addr_externalTemp, ABB.addr_maxLeakageCurrent};

void readInverter(string inverter_IP, int inverter_addr, int inverter_number) {
  pinMode (2, OUTPUT);
  digitalWrite(2, LOW);
  char buf[512];
  Serial.begin(9600);
  // ip address (string port), uint16_t port
  modbus mb = modbus(inverter_IP, 502);
// mb.modbus_set_slave_id(inverter_addr);
  mb.modbus_connect();
  uint16_t a[2];
  uint16_t b[2];
   //int address, int amount, uint16_t *buffer;
  mb.modbus_read_holding_registers(ABB.addr_energy, 2, a);
  b[0] = a[1];
  b[1] = a[0];
  memcpy(&ABB.energy, b, sizeof(b));

  for (int i = 0; i <total_variables; i++) {
        mb.modbus_read_holding_registers(all_addr[i], 2, a);
        b[0] = a[1];
        b[1] = a[0];
        memcpy(&all_variables[i], b, sizeof(b));
  }

  printf("%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf", ABB.energy, ABB.meanOutputCurrent, ABB.phaseACurrent, ABB.phaseBCurrent, ABB.phaseCCurrent, ABB.phaseVoltageAB, ABB.phaseVoltageBC, ABB.phaseVoltageCA, ABB.phaseVoltageAN, ABB.phaseVoltageBN, ABB.phaseVoltageCN, ABB.outputActivePowerInstant, ABB.reactivePower, ABB.apparentPower, ABB.powerFactor, ABB.meanGridFrequency, ABB.inputCurrent, ABB.activePowerDCInput1, ABB.activePowerDCInput2, ABB.activePowerDCInput3, ABB.voltageDCInput1, ABB.voltageDCInput2, ABB.voltageDCInput3, ABB.currentDCInput1, ABB.currentDCInput2, ABB.currentDCInput3, ABB.cabinetTemp, ABB.heatSinkTemp, ABB.transformerTemp, ABB.externalTemp, ABB.maxLeakageCurrent);

  mb.modbus_close();
  Serial.flush();
  digitalWrite(2, HIGH);
  delay(3000);
  // if address is wrong will trigger exception code 02
}

int main(int argc, char **argv) {
  while (1) {
    // send string inverter_IP, int inverter_addr, inverter_number
    readInverter("192.168.1.1", 1, 1);
    delay(1000);
    //readInverter("10.21.37", 2, 1);
    //delay(1000);
    //readInverter("10.21.37.191", 2, 1);
    //delay(1000);
    // Depends how many inverters there are
    // Choose to read at a certain time
  }
  return 0;
}
