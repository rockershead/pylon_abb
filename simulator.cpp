#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <json-c/json.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
//#include <termios.h> /* POSIX Terminal Control Definitions */

void  serial_sender(char write_buffer[])
{


  int fd;
/*------------------------------- Opening the Serial Port -------------------------------*/

                /* Change /dev/ttyUSB0 to the one corresponding to your system */

                fd = open("/dev/ttyAMA0",O_RDWR | O_NOCTTY | O_NDELAY);   /* ttyUSB0 is the FT232 based USB2SERIAL Converter   */
                                                                        /* O_RDWR Read/Write access to serial port           */
                                                                        /* O_NOCTTY - No terminal will control the process   */
                                                                        /* O_NDELAY -Non Blocking Mode,Does not care about-  */
                                                                        /* -the status of DCD line,Open() returns immediatly */

                if(fd == -1)                                            /* Error Checking */
                   printf("\n  Error! in Opening ttyAMA0  ");
                else
                   printf("\n  ttyAMA0 Opened Successfully ");



//writing to serial port//////////////////////////////////////////////////////////////////////////////////////

 // char write_buffer[]  Buffer containing characters to write into port           */
                int  bytes_written  = 0;        /* Value for storing the number of bytes written to the port */

                bytes_written = write(fd,write_buffer,strlen(write_buffer));/* use write() to send data to port                                            */
                                                                             /* "fd"                   - file descriptor pointing to the opened serial port */
                                                                             /* "write_buffer"         - address of the buffer containing data              */
                                                                             /* "sizeof(write_buffer)" - No of bytes to write                               */
               // printf("\n  %s written to ttyS0",write_buffer);
                printf("\n  %d Bytes written to ttyAMA0", bytes_written);
               // printf("\n +----------------------------------+\n\n");

                close(fd);/* Close the Serial port */

}





int randoms(int lower, int upper)
{



       int   num = (rand() %
           (upper - lower + 1)) + lower;
      return num;


}

// Driver code
int main()
{


while(1){
srand(time(0));
json_object *inverter = json_object_new_object();

json_object *dailyEnergy=json_object_new_int(randoms(0, 10000));
json_object *totalEnergy=json_object_new_int(randoms(0, 10000));
json_object *monthlyEnergy=json_object_new_int(randoms(0, 10000));
json_object *yearlyEnergy=json_object_new_int(randoms(0, 10000));
json_object *meanGridVoltage=json_object_new_double(randoms(0, 10000));
json_object *meanOutputCurrent=json_object_new_double(randoms(0, 10000));
json_object *outputActivePowerInstant=json_object_new_double(randoms(0, 10000));
json_object *outputActivePowerAbsolutePeak=json_object_new_double(randoms(0, 10000));
json_object *outputActivePowerDailyPeak=json_object_new_double(randoms(0, 10000));
json_object *reactivePower=json_object_new_double(randoms(0, 10000));
json_object *powerFactor=json_object_new_double(randoms(0, 10000));
json_object *meanGridFrequency=json_object_new_double(randoms(0, 10000));
json_object *activePowerDCInput1=json_object_new_double(randoms(0, 10000));
json_object *voltageDCInput1=json_object_new_double(randoms(0, 10000));
json_object *currentDCInput1=json_object_new_double(randoms(0, 10000));
json_object *activePowerDCInput2=json_object_new_double(randoms(0, 10000));
json_object *voltageDCInput2=json_object_new_double(randoms(0, 10000));
json_object *currentDCInput2=json_object_new_double(randoms(0, 10000));
json_object *activePowerDCInput3=json_object_new_double(randoms(0, 10000));
json_object *voltageDCInput3=json_object_new_double(randoms(0, 10000));
json_object *currentDCInput3=json_object_new_double(randoms(0, 10000));
json_object *internalTemp=json_object_new_double(randoms(0, 10000));
json_object *inverterTemp=json_object_new_double(randoms(0, 10000));
json_object *maxLeakageCurrent=json_object_new_double(randoms(0, 10000));



//display data in json format

json_object_object_add(inverter,"meanGridVoltage", meanGridVoltage);
json_object_object_add(inverter,"dailyEnergy", dailyEnergy);
json_object_object_add(inverter,"totalEnergy", totalEnergy);
json_object_object_add(inverter,"monthlyEnergy", monthlyEnergy);
json_object_object_add(inverter,"yearlyEnergy", yearlyEnergy);
json_object_object_add(inverter,"meanOutputCurrent", meanOutputCurrent);
json_object_object_add(inverter,"outputActivePowerInstant", outputActivePowerInstant);
json_object_object_add(inverter,"outputActivePowerAbsolutePeak", outputActivePowerAbsolutePeak);
json_object_object_add(inverter,"outputActivePowerDailyPeak", outputActivePowerDailyPeak);
json_object_object_add(inverter,"reactivePower", reactivePower);
json_object_object_add(inverter,"powerFactor", powerFactor);
json_object_object_add(inverter,"meanGridFrequency", meanGridFrequency);
json_object_object_add(inverter,"activePowerDCInput1", activePowerDCInput1);
json_object_object_add(inverter,"voltageDCInput1", voltageDCInput1);
json_object_object_add(inverter,"currentDCInput1", currentDCInput1);
json_object_object_add(inverter,"activePowerDCInput2", activePowerDCInput2);
json_object_object_add(inverter,"voltageDCInput2", voltageDCInput2);
json_object_object_add(inverter,"currentDCInput2", currentDCInput2);
json_object_object_add(inverter,"activePowerDCInput3", activePowerDCInput3);
json_object_object_add(inverter,"voltageDCInput3", voltageDCInput3);
json_object_object_add(inverter,"currentDCInput3", currentDCInput3);
json_object_object_add(inverter,"internalTemp", internalTemp);
json_object_object_add(inverter,"inverterTemp", inverterTemp);
json_object_object_add(inverter,"maxLeakageCurrent", maxLeakageCurrent);



printf ("%s",json_object_to_json_string(inverter));
serial_sender(json_object_to_json_string(inverter));

sleep(5);

fflush(stdout);



}




return 0;
}
