#include "modbus.h"
#include "arduPi.h"
#include <string>

using namespace std;

string inverter_IP;
int inverter_addr;
int inverter_number;
int total_variables = 24;

typedef struct ABB_Inverter {
  uint32_t dailyEnergy;                   uint32_t addr_dailyEnergy = 10700;
  uint32_t totalEnergy;                   uint32_t addr_totalEnergy = 1072;
  uint32_t monthlyEnergy;                 uint32_t addr_monthlyEnergy = 1076;
  uint32_t yearlyEnergy;                  uint32_t addr_yearlyEnergy = 1080;
  double meanGridVoltage;                 uint32_t addr_meanGridVoltage = 1090;
  double meanOutputCurrent;               uint32_t addr_meanOutputCurrent = 1092;
  double outputActivePowerInstant;        uint32_t addr_outputActivePowerInstant = 1094;
  double outputActivePowerAbsolutePeak;   uint32_t addr_outputActivePowerAbsolutePeak = 1096;
  double outputActivePowerDailyPeak;      uint32_t addr_outputActivePowerDailyPeak = 1098;
  double reactivePower;                   uint32_t addr_reactivePower = 1102;
  double powerFactor;                     uint32_t addr_powerFactor = 1104;
  double meanGridFrequency;               uint32_t addr_meanGridFrequency = 1106;
  double activePowerDCInput1;             uint32_t addr_activePowerDCInput1 = 1108;
  double voltageDCInput1;                 uint32_t addr_voltageDCInput1 = 1110;
  double currentDCInput1;                 uint32_t addr_currentDCInput1 = 1112;
  double activePowerDCInput2;             uint32_t addr_activePowerDCInput2 = 1114;
  double voltageDCInput2;                 uint32_t addr_voltageDCInput2 = 1116;
  double currentDCInput2;                 uint32_t addr_currentDCInput2 = 1118;
  double activePowerDCInput3;             uint32_t addr_activePowerDCInput3 = 1130;
  double voltageDCInput3;                 uint32_t addr_voltageDCInput3 = 1132;
  double currentDCInput3;                 uint32_t addr_currentDCInput3 = 1134;
  double internalTemp;                    uint32_t addr_internalTemp = 1120;
  double inverterTemp;                    uint32_t addr_inverterTemp = 1122;
  double maxLeakageCurrent;               uint32_t addr_maxLeakageCurrent = 11280;
} Inverter;
Inverter ABB;

unsigned int all_addr[24] = {ABB.addr_dailyEnergy, ABB.addr_totalEnergy, ABB.addr_monthlyEnergy, ABB.addr_yearlyEnergy, ABB.addr_meanOutputCurrent, ABB.addr_meanGridVoltage, ABB.addr_outputActivePowerInstant, ABB.addr_outputActivePowerAbsolutePeak, ABB.addr_outputActivePowerDailyPeak, ABB.addr_reactivePower, ABB.addr_powerFactor, ABB.addr_meanGridFrequency, ABB.addr_activePowerDCInput1, ABB.addr_activePowerDCInput2, ABB.addr_activePowerDCInput3, ABB.addr_voltageDCInput1, ABB.addr_voltageDCInput2, ABB.addr_voltageDCInput3, ABB.addr_currentDCInput1, ABB.addr_currentDCInput2, ABB.addr_currentDCInput3, ABB.addr_internalTemp, ABB.addr_inverterTemp, ABB.addr_maxLeakageCurrent};

double double_variables[20] = {ABB.meanOutputCurrent, ABB.meanGridVoltage, ABB.outputActivePowerInstant, ABB.outputActivePowerAbsolutePeak, ABB.outputActivePowerDailyPeak, ABB.reactivePower, ABB.powerFactor, ABB.meanGridFrequency, ABB.activePowerDCInput1, ABB.activePowerDCInput2, ABB.activePowerDCInput3, ABB.voltageDCInput1, ABB.voltageDCInput2, ABB.voltageDCInput3, ABB.currentDCInput1, ABB.currentDCInput2, ABB.currentDCInput3, ABB.internalTemp, ABB.inverterTemp, ABB.maxLeakageCurrent};

uint32_t int_variables[4] = {ABB.dailyEnergy, ABB.totalEnergy, ABB.monthlyEnergy, ABB.yearlyEnergy};

void readInverter(string inverter_IP, int inverter_addr, int inverter_number) {
  pinMode (2, OUTPUT);
  digitalWrite(2, LOW);
  char buf[512];
  Serial.begin(9600);
  // ip address (string port), uint16_t port
  modbus mb = modbus(inverter_IP, 502);
mb.modbus_set_slave_id(inverter_addr);
  mb.modbus_connect();
  uint16_t a[2];
  uint16_t b[2];
  // int address, int amount, uint16_t *buffer
//  mb.modbus_read_holding_registers(ABB.addr_dailyEnergy, 2, a);
//  b[0] = a[1];
 // b[1] = a[0];
 // memcpy(&ABB.dailyEnergy, b, sizeof(b));

  for (int i = 0; i <total_variables; i++) {
        mb.modbus_read_holding_registers(all_addr[i], 2, a);
        if (i == 0 || i == 1 || i == 2 || i == 3) {
                b[0] = a[1];
                b[1] = a[0];
                memcpy(&int_variables[i], b, sizeof(b));
        }
        else {
                b[0] = a[1];
                b[1] = a[0];
                memcpy(&double_variables[i], b, sizeof(b));
        }
  }

  printf("%u,%u,%u,%u,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf", ABB.dailyEnergy, ABB.totalEnergy, ABB.monthlyEnergy, ABB.yearlyEnergy, ABB.meanOutputCurrent, ABB.meanGridVoltage, ABB.outputActivePowerInstant, ABB.outputActivePowerAbsolutePeak, ABB.outputActivePowerDailyPeak, ABB.reactivePower, ABB.powerFactor, ABB.meanGridFrequency, ABB.activePowerDCInput1, ABB.activePowerDCInput2, ABB.activePowerDCInput3, ABB.voltageDCInput1, ABB.voltageDCInput2, ABB.voltageDCInput3, ABB.currentDCInput1, ABB.currentDCInput2, ABB.currentDCInput3, ABB.internalTemp, ABB.inverterTemp, ABB.maxLeakageCurrent);

  mb.modbus_close();
  Serial.flush();
  digitalWrite(2, HIGH);
  delay(3000);
  // if address is wrong will trigger exception code 02
}

int main(int argc, char **argv) {
  while (1) {
    // send string inverter_IP, int inverter_addr, inverter_number
    readInverter("192.168.1.1", 2, 1);
    delay(1000);

    // Depends how many inverters there are
    // Choose to read at a certain time
  }
  return 0;}
